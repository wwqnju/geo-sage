A Geographical Sparse Additive Generative Model for Spatial Item Recommendation

Geo-SAGE leverages sparse additive model to recommend point of interests （POI） not only in-town but also out-of-town.
The details please refer to KDD 2015 paper "Geo-SAGE: A Geographical Sparse Additive Generative Model for Spatial Item Recommendation". The implementation is based on [standford nlp](http://stanfordnlp.github.io/CoreNLP/) library, we include it in the lib directory.

For a quick start, you may run the main function in the model/GeoGMNew.java

If the code is used in academic publication, please cite with following bibtex item:
```
#!html

@inproceedings{DBLP:conf/kdd/WangYCSSZ15,
               author    = {Weiqing Wang and Hongzhi Yin and Yizhou Sun and Shazia Wasim Sadiq and  Xiaofang Zhou},
               title     = {Geo-SAGE: {A} Geographical Sparse Additive Generative Model for Spatial Item Recommendation},
               booktitle = {KDD},
               pages     = {1255--1264},
               year      = {2015}
}

```